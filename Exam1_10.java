/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.algormid;

import java.util.Scanner;

/**
 *
 * @author acer
 */
public class Exam1_10 {

    public static void main(String[] args) {
        Scanner mn = new Scanner(System.in);
        long startTime = System.nanoTime();
        int n = mn.nextInt();                                                                                                   // 3 
        int arr[] = new int[n];                                                                                                 // 1 2 3
        InputlenArray(n, arr, mn);                                                                                          
        
        LenSubArray(n,arr);
        time(startTime);
    }

    public static void InputlenArray(int n, int arr[], Scanner mn) {
        for (int i = 0; i < n; i++) {
            arr[i] = mn.nextInt();
        }
    }

    public static void LenSubArray(int n, int arr[]) {
        int max = 1;                                                                                                    
        int len = 1;
        int maxindex = 0;
        
        for (int i = 1; i < n; i++){
         if (arr[i] > arr[i-1]){
                len++;
            }else{
                if ( max < len){
                    max = len;
                    maxindex = i - max;
                    }
                
                len = 1;
            }
        }
        if (max < len){
            max = len;
            maxindex = n - max;
        }
        for (int i = maxindex; i < max+maxindex; i++)
            System.out.print(arr[i] + " ");
    }
    
    public static void time(long startTime){
        long endTime = System.nanoTime();
        long TotalTime = endTime - startTime;
        double seconds = (double)TotalTime / 1000000;
        System.out.println("\nTotalTime a Program " + seconds);
    }
}
